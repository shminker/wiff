#! /bin/bash

now=$(date)
urls='www.google.com www.uea.ac.uk www.cmp.uea.ac.uk'

echo $now

for url in $urls
do
	echo $url
	traceroute -n $url
done

echo ''
echo ''
